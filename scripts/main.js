// ES6 import (just because we can)
import React from 'react';
import ReactDOM from 'react-dom';
import _ from 'underscore';
// my own helper functions
import h from './helpers';
// to measure performance
import Perf from 'react-addons-perf';
// to use immutable state
import update from 'react-addons-update';
// to automagically improve performance by only updating components that have changed
import PureRenderMixin from 'react-addons-pure-render-mixin';

// add Perf to window to use it on the console:
// Perf.start() // do something, then stop
// Perf.stop() // show components that are slow with
// Perf.printWasted()
window.Perf = Perf;

// Regarding ES5 => ES6
// With ES6 a lot of syntactic sugar is added (arrow functions and destructuring).
// In function changeTurn (in Tactics component) we explain once how this works. Both ES5 and ES6
// code is written there. Throughout the rest of the code we try to use ES6 where it shortens the code.

/*
  Tactics (main app)
  This will let us make <Tactics/>
*/

var Tactics = React.createClass({
	mixins: [PureRenderMixin],

	/* No PropTypes because this is the main app which will be based on the State */

	getInitialState: function() {
		return {
			data: {
				startTurn: false,
				gameTurn: 1,
				gameFinished: false,
				player1: {
					name: 'Player 1',
					roundsWon: 0,
					turn: true,
					score: h.getCleanScore()
				},
				player2: {
					name: 'Player 2',
					roundsWon: 0,
					turn: false,
					score: h.getCleanScore()
				}
			}
			/*
			data: {
				startTurn: false,
				gameTurn: 1,
				gameFinished: false,
				player1: {
					name: 'Player 1',
					roundsWon: 2,
					turn: true,
					score: {
						'20': {hits: 2, completed: true},
						'19': {hits: 3, completed: true},
						'18': {hits: 3, completed: true},
						'17': {hits: 3, completed: true},
						'16': {hits: 3, completed: true},
						'15': {hits: 3, completed: true},
						'14': {hits: 3, completed: true},
						'13': {hits: 3, completed: true},
						'12': {hits: 3, completed: true},
						'11': {hits: 3, completed: true},
						'10': {hits: 3, completed: true}
					}
				},
				player2: {
					name: 'Player 2',
					roundsWon: 0,
					turn: false,
					score: {
						'20': {hits: 4, completed: false},
						'19': {hits: 3, completed: true},
						'18': {hits: 3, completed: true},
						'17': {hits: 3, completed: true},
						'16': {hits: 4, completed: true},
						'15': {hits: 5, completed: true},
						'14': {hits: 3, completed: true},
						'13': {hits: 4, completed: true},
						'12': {hits: 3, completed: true},
						'11': {hits: 3, completed: true},
						'10': {hits: 3, completed: true}
					}
				}
			}
			*/
		};
	},

	changeTurn: function() {

	    // FULL FLEDGED ES6
	    this.setState(({data}) => ({
	    	data: update(data, {
		    	gameTurn: {$apply: gameturn => 3 - gameturn},
		    	player1: {turn: {$apply: value => !value}},
		    	player2: {turn: {$apply: value => !value}},
    		})
	    }));

	    // DON't REMOVE
	    /* ES5 version with a little ES6 already under the hood
	    this.setState(function(prevState) {
	    	var data = prevState.data;
	    	return {
	    		data: update(data, {
			    	gameTurn: {$apply: gameturn => 3 - gameturn},
			    	player1: {turn: {$apply: value => !value}},
			    	player2: {turn: {$apply: value => !value}},
	    		})
	    	}
	    });
	    */

	},

	hitNumber: function(number, active) {
		if (active) {

			this.setState(function(prevState) {
				var data = prevState.data;
				var player = h.getPlayer(data, 'active');
				if (player.score[number].hits < 3) {

					// Update state based on these booleans
					var activePlayerKey = h.getPlayerKey(data, 'active');
					var otherPlayerKey = h.getPlayerKey(data, 'other');

					// Create newData, the immutable datastructure that will hold the new state, then fill it.
					var newData;

					// Find out if the game is finished with this hit
					if (h.gameFinished(data, number, player)) {
						var points1 = h.calculateScore(player.score);
						var points2 = h.calculateScore(data[otherPlayerKey].score);
						if (points1 > points2) {
							newData = update(data, {
								[activePlayerKey]: {
									score: {
										[number]: {
											hits: {$apply: hits => hits+1}
										}
									},
									roundsWon: {$apply: won => won + 1}
								},
								[otherPlayerKey]: {score: {[number]: {completed: {$apply: value => !value}}}},
								gameFinished: {$apply: value => !value}
							});
						} else if (points1 < points2) {
							newData = update(data, {
								[activePlayerKey]: {score: {[number]: {hits: {$apply: hits => hits+1}}}},
								[otherPlayerKey]: {
									score: {
										[number]: {
											completed: {$apply: value => !value}
										}
									},
									roundsWon: {$apply: won => won + 1}
								},
								gameFinished: {$apply: value => !value}
							});
						}
					} else if (player.score[number].hits === 2) {
						newData = update(data, {
							[activePlayerKey]: {score: {[number]: {hits: {$apply: hits => hits+1}}}},
							[otherPlayerKey]: {score: {[number]: {completed: {$apply: value => !value}}}}
						});
					} else {
						newData = update(data, {
							[activePlayerKey]: {score: {[number]: {hits: {$apply: hits => hits+1}}}}
						});
					}

					return {
						data: newData
					}
				} else {
					// state not to be changed
					return prevState;
				}
			});

		}
	},

	addPoints: function(number) {
		this.setState(({data}) => {
			var playerKey = h.getPlayerKey(data, 'active');
			return {
				data: update(data, {
					[playerKey]: {
						score: {
							[number]: {
								hits: {$apply: hits => hits + 1}
							}
						}
					}
				})
			}
		});
	},

	// reset state for a new game
	resetGame: function() {

		this.setState(({data}) => (
		{
			data: update(data, {
				startTurn: {$apply: value => !value},
				gameFinished: {$apply: value => !value},
				gameTurn: {$apply: value => (1 + value)},
				player1: {
					turn: {$apply: value => data.startTurn},
					score: {$set: h.getCleanScore()}
				},
				player2: {
					turn: {$apply: value => !data.startTurn},
					score: {$set: h.getCleanScore()}
				}
			})
		}));
	},

	render: function() {
		var data = this.state.data;

		// Figure out if game is finished
		var finished = '';
		if (data.gameFinished) {
			finished = <Finished player1={data.player1} player2={data.player2} resetGame={this.resetGame}/>;
		}

		return (
			<div className="tactics">
				<Header tagline="Tactics"/>
				<TacticsGame player1={data.player1} player2={data.player2} changeTurn={this.changeTurn} hitNumber={this.hitNumber} addPoints={this.addPoints}/>
				{finished}
			</div>
		)
	}
});

/*
	Header
	<Header />
 */

var Header = React.createClass({
	mixins: [PureRenderMixin],

	propTypes: {
		tagline: React.PropTypes.string.isRequired
	},

	render: function() {
		return (
			<header>
				<nav className="navbar navbar-default">
					<div className="container-fluid">
						<div className="navbar-header">
							<button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-6" aria-expanded="false">
								<span className="sr-only">Toggle navigation</span>
								<span className="icon-bar"></span>
								<span className="icon-bar"></span>
								<span className="icon-bar"></span>
							</button>
							<a className="navbar-brand" href="#">{this.props.tagline}</a>
						</div>
						<div className="collapse navbar-collapse" id="bs-example-navbar-collapse-6">
							<ul className="nav navbar-nav">
								<li className="active"><a href="#">Home (main menu)</a></li>
								<li><a href="#">Settings</a></li>
								<li><a href="#">Help</a></li>
							</ul>
						</div>
					</div>
				</nav>
			</header>
		)
	}
});

/*
	TacticsGame component: is where the actual game will live
	<TacticsGame />
 */

var TacticsGame = React.createClass({
	mixins: [PureRenderMixin],

	propTypes: {
		player1: React.PropTypes.object.isRequired,
		player2: React.PropTypes.object.isRequired,
		changeTurn: React.PropTypes.func.isRequired,
		hitNumber: React.PropTypes.func.isRequired,
		addPoints: React.PropTypes.func.isRequired
	},

	render: function() {
		var points = 56;
		return (
			<div className="tactics-container">
				<div className="gamefields">
					<PlayerField player={this.props.player1} hitNumber={this.props.hitNumber} addPoints={this.props.addPoints}/>
					<PlayerTurner changeTurn={this.props.changeTurn}/>
					<PlayerField player={this.props.player2} hitNumber={this.props.hitNumber} addPoints={this.props.addPoints}/>
				</div>
				<Standings wonPlayer1={this.props.player1.roundsWon} wonPlayer2={this.props.player2.roundsWon}/>
			</div>
		)
	}
});

/*
	PlayerField component: interface for a single player
	<PlayerField />
 */

var PlayerField = React.createClass({
	mixins: [PureRenderMixin],

	propTypes: {
		player: React.PropTypes.object.isRequired,
		hitNumber: React.PropTypes.func.isRequired,
		addPoints: React.PropTypes.func.isRequired
	},

	renderRow: function(item) {
		return <Row key={item} number={item} hits={this.props.player.score[item].hits} completedByOpponent={this.props.player.score[item].completed} turn={this.props.player.turn} hitNumber={this.props.hitNumber} addPoints={this.props.addPoints}/>
	},

	render: function() {
		var gameClass = 'gamefield';
		if (this.props.player.turn) {
			gameClass += ' turn';
		}
		/* Compute the points sofar for this player */
		var points = h.calculateScore(this.props.player.score);

		return (
			<div className={gameClass}>
				<h2>{this.props.player.name} (<span className="player-points">{points}</span>)</h2>
				{_.range(20,9,-1).map(this.renderRow)}
			</div>
		)
	}
});

/*
	Row component: to display a row with players progress in the game for a single number
	<Row />
 */

var Row = React.createClass({
	mixins: [PureRenderMixin],

	propTypes: {
		number: React.PropTypes.number.isRequired,
		hitNumber: React.PropTypes.func.isRequired,
		hits: React.PropTypes.number.isRequired,
		completedByOpponent: React.PropTypes.bool.isRequired,
		addPoints: React.PropTypes.func.isRequired,
		turn: React.PropTypes.bool.isRequired
	},

	render: function() {
		var cntrClass = 'tactics-row';
		var status = 'hidden';
		if (this.props.hits >= 3) {
			cntrClass += ' row-completed';
			status = '';
		}
		var itemClass = 'row-item hit-' + Math.min(3, this.props.hits);

		return (
			<div className={cntrClass}>
				<span className={itemClass} data-item="1" onClick={this.props.hitNumber.bind(null, this.props.number, this.props.turn)}>{this.props.number}</span>
				<span className={itemClass} data-item="2" onClick={this.props.hitNumber.bind(null, this.props.number, this.props.turn)}>{this.props.number}</span>
				<span className={itemClass} data-item="3" onClick={this.props.hitNumber.bind(null, this.props.number, this.props.turn)}>{this.props.number}</span>
				<PointsCounter amount={Math.max(0, this.props.number*(this.props.hits - 3))} status={status} completedByOpponent={this.props.completedByOpponent} number={this.props.number} addPoints={this.props.addPoints}/>
			</div>
		)
	}
});

/*
	PointsCounter component: displayed when Row completed (and other player not) to count the points
	<PointsCounter />
 */

var PointsCounter = React.createClass({
	mixins: [PureRenderMixin],

	propTypes: {
		status: React.PropTypes.string.isRequired,
		amount: React.PropTypes.number.isRequired,
		completedByOpponent: React.PropTypes.bool.isRequired,
		number: React.PropTypes.number.isRequired,
		addPoints: React.PropTypes.func.isRequired
	},

	render: function() {
		var pointsClass = 'points-container row-item ' + this.props.status;
		var points = <span className="points-amount">{this.props.amount}</span>
		var button = '';
		if (!this.props.completedByOpponent) {
			button = <button type="button" onClick={this.props.addPoints.bind(null, this.props.number)}><span className="glyphicon glyphicon-plus"></span></button>
		}

		return (
			<span className={pointsClass}>
				{button}
				{points}
			</span>
		)
	}
});

/*
	PlayerTurner component: component to update turns
	<PlayerTurner />
 */

var PlayerTurner = React.createClass({
	mixins: [PureRenderMixin],

	propTypes: {
		changeTurn: React.PropTypes.func.isRequired
	},

	render: function() {
		return (
			<div className="turner-container">
				<div className="turner" onClick={this.props.changeTurn}>
					<img src="/build/css/images/icon-24-one-finger-tap.png"/>
				</div>
			</div>
		)
	}
});

/*
	Standings component: small container for standings in the game
	<Standings />
 */

var Standings = React.createClass({
	mixins: [PureRenderMixin],

	propTypes: {
		wonPlayer1: React.PropTypes.number.isRequired,
		wonPlayer2: React.PropTypes.number.isRequired
	},

	render: function() {
		return (
			<div className="standings">
				<h2>{this.props.wonPlayer1} - {this.props.wonPlayer2}</h2>
			</div>
		)
	}
});

/*
	Finished component: small overlay popup when game is finished
	<Finished />
 */

var Finished = React.createClass({
	mixins: [PureRenderMixin],

	propTypes: {
		player1: React.PropTypes.object.isRequired,
		player2: React.PropTypes.object.isRequired,
		resetGame: React.PropTypes.func.isRequired
	},

	render: function() {
		var points1 = h.calculateScore(this.props.player1.score);
		var points2 = h.calculateScore(this.props.player2.score);

		return (
			<div className="overlay-container">
				<div className="popup">
					<h2>Game shot</h2>
					<div className="popup-standings">
						<div className="row">
							<div className="col-md-4">{this.props.player1.name}</div>
							<div className="col-md-4">{points1}</div>
							<div className="col-md-4">{this.props.player1.roundsWon}</div>
						</div>
						<div className="row">
							<div className="col-md-4">{this.props.player2.name}</div>
							<div className="col-md-4">{points2}</div>
							<div className="col-md-4">{this.props.player2.roundsWon}</div>
						</div>
					</div>

					<div className="popup-buttons">
						<button type="button" className="btn btn-warning" onClick={this.props.resetGame}><span className="glyphicon glyphicon-play-circle"></span> Play again</button>
						<button type="button" className="btn btn-default"><span className="glyphicon glyphicon-menu-hamburger"></span> Menu</button>
					</div>
				</div>
			</div>
		)
	}
});

ReactDOM.render(<Tactics/>, document.querySelector('#main'));
